package com.danit.bank.dao;

import com.danit.bank.model.Account;
import com.danit.bank.model.Currency;
import com.danit.bank.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Component
public class CustomerDao implements Dao<Customer> {

    private final List<Customer> list = new ArrayList<>();
    ;

    {
        list.add(new Customer("Artem", "test1@mail.ru", 25));
        list.add(new Customer("Mark", "test2@mail.ru", 60));
        list.add(new Customer("Inna", "test3@mail.ru", 18));
        list.add(new Customer("Ivan", "test4@mail.ru", 30));
        list.add(new Customer("Mike", "test5@mail.ru", 29));
    }

    @Override
    public Customer save(Customer obj) {
        list.add(obj);
        return obj;
    }

    public void edit(Long id, Customer obj) {
        int index = list.indexOf(list.stream().filter(i -> i.getId().equals(id)).findFirst().orElse(null));
        list.set(index, obj);
    }


    @Override
    public boolean delete(Customer obj) {
        if (list.contains(obj)) {
            list.remove(obj);
            return true;
        }
        return false;
    }

    @Override
    public void deleteAll(List<Customer> entities) {
        for (Customer c : entities) {
            list.removeIf(i -> c.getId().equals(i.getId()));
        }
    }

    @Override
    public void saveAll(List<Customer> entities) {
        list.addAll(entities);
    }

    @Override
    public List<Customer> findAll() {
        return list;
    }

    @Override
    public boolean deleteById(long id) {
        return list.removeIf(i -> i.getId() == id);
    }

    @Override
    public Customer getOne(long id) {
        return list.stream().filter(i -> i.getId() == id).findFirst().orElse(null);
    }
}
