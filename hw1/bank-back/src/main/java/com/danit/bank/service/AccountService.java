package com.danit.bank.service;

import com.danit.bank.dao.AccountDao;
import com.danit.bank.model.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AccountService {
    public AccountDao accountDao;

    @Autowired
    public AccountService(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    public Account save(Account obj) {
        return accountDao.save(obj);
    }

    public boolean delete(Account obj) {
        return accountDao.delete(obj);
    }

    public void deleteAll(List<Account> entities) {
        accountDao.deleteAll(entities);
    }

    public void saveAll(List<Account> entities) {
        accountDao.saveAll(entities);
    }

    public List<Account> findAll() {
        return accountDao.findAll();
    }

    public boolean deleteById(long id) {
        return accountDao.deleteById(id);
    }

    public Account getOne(long id) {
        return accountDao.getOne(id);
    }

    public List<Account> getByCustomerId(long id) {
        return accountDao.getByCustomerId(id);
    }

    public Account getByNumber(String string) {
        return accountDao.getByNumber(string);
    }


}
