package com.danit.bank.service;

import com.danit.bank.dao.CustomerDao;
import com.danit.bank.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerService {

    public CustomerDao customerDao;

    @Autowired
    public CustomerService(CustomerDao customerDao) {
        this.customerDao = customerDao;
    }

    public List<Customer> findAll() {
        return customerDao.findAll();
    }

    public void edit(Long id, Customer obj) {
        customerDao.edit(id, obj);
    }

    public Customer getOne(long id) {
        return customerDao.getOne(id);
    }

    public boolean deleteById(long id) {
        return customerDao.deleteById(id);
    }

    public Customer save(Customer obj) {
        return customerDao.save(obj);
    }


}
