package com.danit.bank.controller;

import com.danit.bank.model.Account;
import com.danit.bank.model.Customer;
import com.danit.bank.service.AccountService;
import com.danit.bank.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/account")
public class AccountController {
    private AccountService accountService;

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping("/get")
    ResponseEntity hello() {
        return new ResponseEntity<>(accountService.findAll(), HttpStatus.ACCEPTED);
    }

    @GetMapping("/get/{id}")
    ResponseEntity<Account> getAccountById(@PathVariable Long id) {
        return new ResponseEntity<>(accountService.getOne(id), HttpStatus.ACCEPTED);
    }

    @GetMapping("/getByCustomerID/{id}")
    ResponseEntity<List<Account>> getAccountByCustomerId(@PathVariable Long id) {
        return new ResponseEntity<>(accountService.getByCustomerId(id), HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/delete/{id}")
    ResponseEntity<Void> deleteAccountById(@PathVariable Long id) {
        accountService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @PostMapping("/create")
    ResponseEntity<Account> getCustomerById(@RequestBody Account account) {
        return new ResponseEntity<>(accountService.save(new Account(account.getCurrency(), account.getCustomer())), HttpStatus.ACCEPTED);
    }

    @PostMapping("/addmoney")
    ResponseEntity<Account> addMoneyAccount(@RequestParam("accountnumber") String number, @RequestParam("value") Double value) {
        Double oldBalance = accountService.getByNumber(number).getBalance();
        Double newBalance = oldBalance + value;
        accountService.getByNumber(number).setBalance(newBalance);
        return new ResponseEntity<>(accountService.getByNumber(number), HttpStatus.ACCEPTED);
    }

    @PostMapping("/withdraw")
    ResponseEntity<Account> withdrawMoneyAccount(@RequestParam("accountnumber") String number, @RequestParam("value") Double value) {
        if (accountService.getByNumber(number).getBalance() >= value) {
            Double oldBalance = accountService.getByNumber(number).getBalance();
            Double newBalance = oldBalance - value;
            accountService.getByNumber(number).setBalance(newBalance);
            return new ResponseEntity<>(accountService.getByNumber(number), HttpStatus.ACCEPTED);
        } else {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }

    @PostMapping("/transfer")
    ResponseEntity<Account> transferMoneyAccount(@RequestParam("fromaccountnumber") String fromnumber, @RequestParam("toaccountnumber") String tonumber, @RequestParam("value") Double value) {
        if (accountService.getByNumber(fromnumber).getBalance() >= value) {
            accountService.getByNumber(fromnumber).setBalance(accountService.getByNumber(fromnumber).getBalance() - value);
            accountService.getByNumber(tonumber).setBalance(accountService.getByNumber(tonumber).getBalance() + value);
            return new ResponseEntity<>(accountService.getByNumber(tonumber), HttpStatus.ACCEPTED);
        } else {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }


}