package com.danit.bank.model;

import java.util.Objects;
import java.util.UUID;

public class Account {
    private Long id;
    private String number;
    private Currency currency;
    private Double balance;
    private Long customer;

    private static Long ID_COUNT = 0L;

    public Account() {

    }

    public Account(Currency currency, Long customer) {
        this.currency = currency;
        this.customer = customer;
        this.number = UUID.randomUUID().toString();
        id = ++ID_COUNT;
        balance = 0D;
    }

    public Account(Long id, String number, Currency currency, Double balance, Long customer) {
        this.id = id;
        this.number = number;
        this.currency = currency;
        this.balance = balance;
        this.customer = customer;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public Long getCustomer() {
        return customer;
    }

    public void setCustomer(Long customer) {
        this.customer = customer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return Objects.equals(getId(), account.getId()) && Objects.equals(getNumber(), account.getNumber()) && getCurrency() == account.getCurrency() && Objects.equals(getBalance(), account.getBalance()) && Objects.equals(getCustomer(), account.getCustomer());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getNumber(), getCurrency(), getBalance(), getCustomer());
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", number='" + number + '\'' +
                ", currency=" + currency +
                ", balance=" + balance +
                ", customer=" + customer +
                '}';
    }
}