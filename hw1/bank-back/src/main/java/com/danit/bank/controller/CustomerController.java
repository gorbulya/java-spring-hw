package com.danit.bank.controller;

import com.danit.bank.model.Account;
import com.danit.bank.model.Customer;
import com.danit.bank.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/customer")
public class CustomerController {
    private CustomerService customerService;

    @Autowired
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/get")
    ResponseEntity hello() {
        return new ResponseEntity<>(customerService.findAll(), HttpStatus.ACCEPTED);
    }

    @GetMapping("/get/{id}")
    ResponseEntity<Customer> getCustomerById(@PathVariable Long id) {
        return new ResponseEntity<>(customerService.getOne(id), HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/delete/{id}")
    ResponseEntity<Void> deleteCustomerById(@PathVariable Long id) {
        customerService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @PostMapping("/create")
    ResponseEntity<Customer> createCustomer(@RequestBody Customer customer) {
        return new ResponseEntity<>(customerService.save(new Customer(customer.getName(), customer.getEmail(), customer.getAge())), HttpStatus.ACCEPTED);
    }

    @PutMapping("/edit")
    ResponseEntity<Void> editCustomerById(@RequestBody Customer customer) {
        String newName = customer.getName() == null ? customerService.getOne(customer.getId()).getName() : customer.getName();
        String newEmail = customer.getEmail() == null ? customerService.getOne(customer.getId()).getEmail() : customer.getEmail();
        int newAge = customer.getAge() == null ? customerService.getOne(customer.getId()).getAge() : customer.getAge();
        List<Account> newList = customer.getAccounts() == null ? customerService.getOne(customer.getId()).getAccounts() : customer.getAccounts();

        Customer result = new Customer(customer.getId(), newName, newEmail, newAge, newList);

        customerService.edit(customer.getId(), result);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }


}
