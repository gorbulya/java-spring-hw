package com.danit.bank.dao;

import com.danit.bank.model.Account;
import com.danit.bank.model.Currency;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Repository
public class AccountDao implements Dao<Account> {

    private final List<Account> list = new ArrayList<>();

    {
        list.add(new Account(Currency.EUR, 1L));
        list.add(new Account(Currency.GBP, 2L));
        list.add(new Account(Currency.USD, 3L));
        list.add(new Account(Currency.UAH, 3L));
        list.add(new Account(Currency.CHF, 4L));
        list.add(new Account(Currency.EUR, 5L));
        list.add(new Account(Currency.USD, 5L));
        list.add(new Account(Currency.GBP, 5L));

        list.forEach(e -> e.setBalance(20 + Math.random() * 400));

    }

    @Override
    public Account save(Account obj) {
        list.add(obj);
        return obj;
    }

    @Override
    public boolean delete(Account obj) {
        if (list.contains(obj)) {
            list.remove(obj);
            return true;
        }
        return false;
    }

    @Override
    public void deleteAll(List<Account> entities) {
        for (Account c : entities) {
            list.removeIf(i -> c.getId().equals(i.getId()));
        }
    }

    @Override
    public void saveAll(List<Account> entities) {
        list.addAll(entities);
    }

    @Override
    public List<Account> findAll() {
        return list;
    }

    @Override
    public boolean deleteById(long id) {
        return list.removeIf(i -> i.getId() == id);
    }

    @Override
    public Account getOne(long id) {
        return list.stream().filter(i -> i.getId() == id).findFirst().orElse(null);
    }

    public List<Account> getByCustomerId(long id) {
        return list.stream().filter(i -> i.getCustomer() == id).collect(Collectors.toList());
    }

    public Account getByNumber(String string) {
        return list.stream().filter(i -> Objects.equals(i.getNumber(), string)).findFirst().orElse(null);
    }

}
