INSERT into employer (id, adress, name ) values (1, 'Kiev', 'OTP');
INSERT into employer (id, adress, name ) values (2, 'Dnepr', 'Privat');
INSERT into employer (id, adress, name ) values (3, 'Lvov', 'Sber');
INSERT into employer (id, adress, name ) values (4, 'Nikolaev', 'Lobmok');


INSERT into customer (id, age, email, name ) values (1, 25, 'test1.mail.ru', 'Artem');
INSERT into customer (id, age, email, name ) values (2, 35, 'test2.mail.ru' ,'Mike');
INSERT into customer (id, age, email, name ) values (3, 45, 'test3.mail.ru' ,'Nikol');
INSERT into customer (id, age, email, name ) values (4, 50, 'test4.mail.ru' ,'Oleg');

insert into account (number, currency, balance, CUSTOMER_ID) values ('8387783e-a51b-4c13-94c8-0d6bfbd436da', 'EUR', 200, 1);
insert into account (number, currency, balance, CUSTOMER_ID) values ('fdc17651-45b9-446c-b3ad-bfda10cffc85', 'GBP', 300, 2);
insert into account (number, currency, balance, CUSTOMER_ID) values ('38a5528b-f9a3-47dc-ab98-eb25fc55a2c4', 'USD', 340, 3);
insert into account (number, currency, balance, CUSTOMER_ID) values ('ff747308-31db-48bb-ac7f-3f2786772ccb', 'UAH', 250, 3);
insert into account (number, currency, balance, CUSTOMER_ID) values ('e328028b-c83f-4dc7-b73c-55bbb3a23ff8', 'CHF', 500, 4);
