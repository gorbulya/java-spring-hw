package com.danit.bank.service;

import com.danit.bank.dao.CustomerRepository;
import com.danit.bank.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;


@Service
@Transactional
public class CustomerService {

    public CustomerRepository customerRepository;

    @Autowired
    public CustomerService(CustomerRepository customerRepository){
        this.customerRepository = customerRepository;
    }

    public List<Customer> findAll() {
        return customerRepository.findAll();
    }

    public void edit(Long id, Customer obj) {
        customerRepository.Edit(id, obj );
    }

    public Customer getOne(long id) {
        return customerRepository.getOne(id);
    }

    public boolean deleteById(long id) {
        return customerRepository.deleteById(id);
    }

    public Customer save(Customer obj) {
        return customerRepository.save(obj);
    }


}


//@Service
//@Transactional
//public class CustomerService {
//
//    public CustomerDao customerDao;
//
//    @Autowired
//    public CustomerService(CustomerDao customerDao){
//        this.customerDao = customerDao;
//    }
//
//    public List<Customer> findAll() {
//       return customerDao.findAll();
//    }
//
//    public void edit(Long id, Customer obj) {
//        int index = customerDao.findAll().indexOf(customerDao.findAll().stream().filter(i -> i.getId().equals(id)).findFirst().orElse(null));
//        customerDao.findAll().set(index, obj);
//    }
//
//    public Customer getOne(long id) {
//        return customerDao.getOne(id);
//    }
//
//    public boolean deleteById(long id) {
//        return customerDao.deleteById(id);
//    }
//
//    public Customer save(Customer obj) {
//        return customerDao.save(obj);
//    }
//
//
//}
