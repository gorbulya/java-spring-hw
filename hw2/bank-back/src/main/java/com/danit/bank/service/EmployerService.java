package com.danit.bank.service;

import com.danit.bank.dao.EmployerRepository;
import com.danit.bank.model.Employer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Service
public class EmployerService {
    public EmployerRepository employerRepository;

    @Autowired
    public EmployerService(EmployerRepository employerRepository){
        this.employerRepository = employerRepository;
    }

    public Employer save(Employer obj) {
        return employerRepository.save(obj);
    }

    public boolean delete(Employer obj) {
        return employerRepository.delete(obj);
    }

    public void deleteAll(List<Employer> entities) {
        employerRepository.deleteAll(entities);
    }

    public void saveAll(List<Employer> entities) {
        employerRepository.saveAll(entities);
    }

    public List<Employer> findAll() {
        return employerRepository.findAll();
    }

    public boolean deleteById(long id) {
        return employerRepository.deleteById(id);
    }

    public Employer getOne(long id) {
        return employerRepository.getOne(id);
    }

}



//
//@Service
//public class EmployerService {
//    public EmployerDao employerDao;
//
//    @Autowired
//    public EmployerService(EmployerDao employerDao){
//        this.employerDao = employerDao;
//    }
//
//    public Employer save(Employer obj) {
//        return employerDao.save(obj);
//    }
//
//    public boolean delete(Employer obj) {
//        return employerDao.delete(obj);
//    }
//
//    public void deleteAll(List<Employer> entities) {
//        employerDao.deleteAll(entities);
//    }
//
//    public void saveAll(List<Employer> entities) {
//        employerDao.saveAll(entities);
//    }
//
//    public List<Employer> findAll() {
//        return employerDao.findAll();
//    }
//
//    public boolean deleteById(long id) {
//        return employerDao.deleteById(id);
//    }
//
//    public Employer getOne(long id) {
//        return employerDao.getOne(id);
//    }
//
//}
