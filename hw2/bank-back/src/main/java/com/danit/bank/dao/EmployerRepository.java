package com.danit.bank.dao;

import com.danit.bank.model.Employer;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class EmployerRepository implements Dao<Employer> {

    private final EntityManager entityManager;

    public EmployerRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }


    @Override
    public Employer save(Employer obj) {
        entityManager.persist(obj);
        return obj;
    }

    @Override
    public boolean delete(Employer obj) {
        if (entityManager.contains(obj)){
            entityManager.remove(obj);
            return true;
        } else {
            return false;
        }
    }

    @Override
    @Transactional
    public void deleteAll(List<Employer> entities) {
        entities.forEach(entity->{
            entityManager.remove(entity);
        });

    }

    @Override
    @Transactional
    public void saveAll(List<Employer> entities) {
        entities.forEach(entity->{
            entityManager.persist(entity);
        });
    }

    @Override
    public List<Employer> findAll() {
        return entityManager.createQuery("select s from Employer s").getResultList();
    }

    @Override
    @Transactional
    public boolean deleteById(long id) {
        Query query = entityManager.createQuery("delete from Employer where id=:id");
        query.setParameter("id", id);
        int endel = query.executeUpdate();
        return endel>0;
    }


    @Override
    public Employer getOne(long id) {
        return entityManager.find(Employer.class, id);
    }

}



