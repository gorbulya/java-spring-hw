package com.danit.bank.controller;

import com.danit.bank.model.Account;
import com.danit.bank.model.Customer;
import com.danit.bank.service.AccountService;
import com.danit.bank.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/account")
public class AccountController {
    private AccountService accountService;
    private CustomerService customerService;

    @Autowired
    public AccountController(AccountService accountService, CustomerService customerService) {
        this.accountService = accountService;
        this.customerService = customerService;
    }

    @GetMapping("/get")
    ResponseEntity hello() {
        return new ResponseEntity<>(accountService.findAll(), HttpStatus.ACCEPTED);
    }

    @GetMapping("/get/{id}")
    ResponseEntity<Account> getAccountById(@PathVariable Long id) {
        return new ResponseEntity<>(accountService.getOne(id), HttpStatus.ACCEPTED);
    }

    @GetMapping("/getByCustomerID/{id}")
    ResponseEntity<List<Account>> getAccountByCustomerId(@PathVariable Long id) {
        return new ResponseEntity<>(accountService.getByCustomerId(id), HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/delete/{id}")
    ResponseEntity<Void> deleteAccountById(@PathVariable Long id) {
        accountService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @PostMapping("/create/{id}")
    ResponseEntity<Void> getCustomerById(@RequestBody Account account, @PathVariable Long id) {
        Account account1 = new Account();
        account1.setNumber(UUID.randomUUID().toString());
        account1.setCurrency(account.getCurrency());
        account1.setBalance(account.getBalance());

        Customer customer1 = customerService.getOne(id);
        customer1.addAccount(account1);
        customerService.edit(id, customer1);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @PostMapping("/addmoney")
    ResponseEntity<Account> addMoneyAccount(@RequestParam("accountnumber") String number, @RequestParam("value") Double value) {
        Double oldBalance = accountService.getByNumber(number).getBalance();
        Double newBalance = oldBalance+value;
        Account acc = accountService.getByNumber(number);
        acc.setBalance(newBalance);
        accountService.save(acc);
        return new ResponseEntity<>(accountService.getByNumber(number), HttpStatus.ACCEPTED);
    }

    @PostMapping("/withdraw")
    ResponseEntity<Account> withdrawMoneyAccount(@RequestParam("accountnumber") String number, @RequestParam("value") Double value) {
        if (accountService.getByNumber(number).getBalance() >= value){
            Double oldBalance = accountService.getByNumber(number).getBalance();
            Double newBalance = oldBalance-value;
            Account acc = accountService.getByNumber(number);
            acc.setBalance(newBalance);
            return new ResponseEntity<>(accountService.getByNumber(number), HttpStatus.ACCEPTED);
        } else {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }

    @PostMapping("/transfer")
    ResponseEntity<Account> transferMoneyAccount(@RequestParam("fromaccountnumber") String fromnumber, @RequestParam("toaccountnumber") String tonumber, @RequestParam("value") Double value) {
        if (accountService.getByNumber(fromnumber).getBalance() >= value){
            Account accfrom = accountService.getByNumber(fromnumber);
            Double fromvalue = accountService.getByNumber(fromnumber).getBalance();
            Account accto = accountService.getByNumber(tonumber);
            Double tovalue = accountService.getByNumber(tonumber).getBalance();
            accfrom.setBalance(fromvalue-value);
            accto.setBalance(tovalue+value);
            return new ResponseEntity<>(accountService.getByNumber(tonumber), HttpStatus.ACCEPTED);
        } else {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }
}