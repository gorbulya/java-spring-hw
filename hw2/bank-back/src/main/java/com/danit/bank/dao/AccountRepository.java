package com.danit.bank.dao;

import com.danit.bank.model.Account;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class AccountRepository implements Dao<Account>{


    private final EntityManager entityManager;

    public AccountRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }


    @Override
    public Account save(Account obj) {
        entityManager.persist(obj);
        return obj;
    }

    @Override
    public boolean delete(Account obj) {
        if (entityManager.contains(obj)){
            entityManager.remove(obj);
            return true;
        } else {
            return false;
        }
    }

    @Override
    @Transactional
    public void deleteAll(List<Account> entities) {
        entities.forEach(entity->{
            entityManager.remove(entity);
        });

    }

    @Override
    @Transactional
    public void saveAll(List<Account> entities) {
        entities.forEach(entity->{
            entityManager.persist(entity);
        });
    }

    @Override
    public List<Account> findAll() {
        return entityManager.createQuery("select s from Account s").getResultList();
    }

    @Override
    @Transactional
    public boolean deleteById(long id) {
        Query query = entityManager.createQuery("delete from Account where id=:id");
        query.setParameter("id", id);
        int endel = query.executeUpdate();
        return endel>0;
    }


    @Override
    public Account getOne(long id) {
        return entityManager.find(Account.class, id);
    }
}
