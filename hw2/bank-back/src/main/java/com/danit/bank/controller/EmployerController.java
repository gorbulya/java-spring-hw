package com.danit.bank.controller;

import com.danit.bank.model.Employer;
import com.danit.bank.service.EmployerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/employer")
public class EmployerController {
    private EmployerService employerService;

    @Autowired
    public EmployerController(EmployerService employerService) {
        this.employerService = employerService;
    }

    @GetMapping("/get")
    ResponseEntity hello() {
        return new ResponseEntity<>(employerService.findAll(), HttpStatus.ACCEPTED);
    }

    @GetMapping("/get/{id}")
    ResponseEntity<Employer> getEmployerById(@PathVariable Long id) {
        return new ResponseEntity<>(employerService.getOne(id), HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/delete/{id}")
    ResponseEntity<Void> deleteEmployerById(@PathVariable Long id) {
        employerService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @PostMapping("/create")
    ResponseEntity<Employer> getCustomerById(@RequestBody Employer employer) {
        Employer ebb = new Employer();
        ebb.setName(employer.getName());
        ebb.setAdress(employer.getAdress());
        return new ResponseEntity<>(employerService.save(ebb), HttpStatus.ACCEPTED);
    }
}