package com.danit.bank.service;

import com.danit.bank.dao.AccountRepository;
import com.danit.bank.model.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
@Transactional
@Service
public class AccountService {
    public AccountRepository accountRepository;

    @Autowired
    public AccountService(AccountRepository accountRepository){
        this.accountRepository = accountRepository;
    }

    public Account save(Account obj) {
        return accountRepository.save(obj);
    }

    public boolean delete(Account obj) {
        return accountRepository.delete(obj);
    }

    public void deleteAll(List<Account> entities) {
        accountRepository.deleteAll(entities);
    }

    public void saveAll(List<Account> entities) {
        accountRepository.saveAll(entities);
    }

    public List<Account> findAll() {
        return accountRepository.findAll();
    }

    public boolean deleteById(long id) {
        return accountRepository.deleteById(id);
    }

    public Account getOne(long id) {
        return accountRepository.getOne(id);
    }

    public List<Account> getByCustomerId(long id) {
        return accountRepository.findAll().stream().filter(i -> i.getCustomer().getId()==id).collect(Collectors.toList());
    }
    public Account getByNumber(String string) {
        return accountRepository.findAll().stream().filter(i -> Objects.equals(i.getNumber(), string)).findFirst().orElse(null);

    }


}



//@Service
//public class AccountService {
//    public AccountDao accountDao;
//
//    @Autowired
//    public AccountService(AccountDao accountDao){
//        this.accountDao = accountDao;
//    }
//
//    public Account save(Account obj) {
//        return accountDao.save(obj);
//    }
//
//    public boolean delete(Account obj) {
//        return accountDao.delete(obj);
//    }
//
//    public void deleteAll(List<Account> entities) {
//        accountDao.deleteAll(entities);
//    }
//
//    public void saveAll(List<Account> entities) {
//        accountDao.saveAll(entities);
//    }
//
//    public List<Account> findAll() {
//        return accountDao.findAll();
//    }
//
//    public boolean deleteById(long id) {
//        return accountDao.deleteById(id);
//    }
//
//    public Account getOne(long id) {
//        return accountDao.getOne(id);
//    }
//
//    public List<Account> getByCustomerId(long id) {
//        return accountDao.findAll().stream().filter(i -> i.getCustomer() == id).collect(Collectors.toList());
//    }
//    public Account getByNumber(String string) {
//        return accountDao.findAll().stream().filter(i -> Objects.equals(i.getNumber(), string)).findFirst().orElse(null);
//
//    }
//
//
//}
