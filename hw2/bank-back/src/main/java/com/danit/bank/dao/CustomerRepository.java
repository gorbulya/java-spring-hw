package com.danit.bank.dao;

import com.danit.bank.model.Customer;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class CustomerRepository implements Dao<Customer>{


    private final EntityManager entityManager;

    public CustomerRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }


    @Override
    public Customer save(Customer obj) {
        entityManager.persist(obj);
        return obj;
    }

    public Customer Edit(Long id, Customer obj) {
        Customer customer = entityManager.find(Customer.class, id);
        customer.setName(obj.getName());
        customer.setEmail(obj.getEmail());
        customer.setAge(obj.getAge());
        entityManager.persist(customer);
        return customer;
    }


    @Override
    public boolean delete(Customer obj) {
        if (entityManager.contains(obj)){
            entityManager.remove(obj);
            return true;
        } else {
            return false;
        }
    }

    @Override
    @Transactional
    public void deleteAll(List<Customer> entities) {
        entities.forEach(entity->{
            entityManager.remove(entity);
        });

    }

    @Override
    @Transactional
    public void saveAll(List<Customer> entities) {
        entities.forEach(entity->{
            entityManager.persist(entity);
        });
    }

    @Override
    public List<Customer> findAll() {
        return entityManager.createQuery("select s from Customer s").getResultList();
    }

    @Override
    public boolean deleteById(long id) {
        Query query = entityManager.createQuery("delete from Customer where id=:id");
        query.setParameter("id", id);
        int endel = query.executeUpdate();
        return endel>0;
    }


    @Override
    public Customer getOne(long id) {
        return entityManager.find(Customer.class, id);
    }
}
