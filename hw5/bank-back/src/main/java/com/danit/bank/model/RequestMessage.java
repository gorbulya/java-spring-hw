package com.danit.bank.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class RequestMessage {
    private String payload;
    private String topicURI;
}
