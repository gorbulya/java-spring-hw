package com.danit.bank.controller;

import com.danit.bank.dto.EmployerRequest;
import com.danit.bank.dto.EmployerResponse;
import com.danit.bank.exception.EmployerNotFound;
import com.danit.bank.model.Employer;
import com.danit.bank.service.EmployerService;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("/employers")
public class EmployerController {
    private EmployerService employerService;
    private ModelMapper modelMapper;

    public EmployerController(EmployerService employerService, ModelMapper modelMapper) {
        this.employerService = employerService;
        this.modelMapper = modelMapper;
    }

    @GetMapping
    ResponseEntity hello() {
        List<EmployerResponse> er = employerService.findAll().stream()
                .map(e->modelMapper.map(e,EmployerResponse.class))
                .collect(Collectors.toList());
        log.info("Find all employers");
        return new ResponseEntity<>(er, HttpStatus.ACCEPTED);
    }

    @GetMapping("/{id}")
    ResponseEntity<EmployerResponse> getEmployerById(@PathVariable Long id) throws EmployerNotFound {
        EmployerResponse er = modelMapper.map(employerService.getOne(id), EmployerResponse.class);
        log.info("Find employer by {} number", id);
        return new ResponseEntity<>(er, HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/{id}")
    ResponseEntity<Void> deleteEmployerById(@PathVariable Long id) {
        employerService.deleteById(id);
        log.info("Delete employer by {} number", id);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @PostMapping
    ResponseEntity<Employer> getCustomerById(@RequestBody @Valid EmployerRequest employerRequest) {
        Employer employer = modelMapper.map(employerRequest, Employer.class);
        Employer ebb = new Employer();
        ebb.setName(employer.getName());
        ebb.setAddress(employer.getAddress());
        log.info("Create new employer");
        return new ResponseEntity<>(employerService.save(ebb), HttpStatus.ACCEPTED);
    }
}