package com.danit.bank.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class MonitoringAspect {

    Logger logger = LoggerFactory.getLogger(MonitoringAspect.class);

    @Pointcut("@annotation(Monitoring)")
    public void pointcut(){}

    @Around("pointcut()")
    public Object aroundLogic(ProceedingJoinPoint pjp) throws Throwable {

        Object proceed = pjp.proceed();
        logger.info("The user has changed the amount of money on the account");
        return proceed;

    }



}
