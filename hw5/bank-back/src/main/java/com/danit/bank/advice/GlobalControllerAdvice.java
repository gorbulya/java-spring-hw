package com.danit.bank.advice;

import com.danit.bank.exception.AccountNotFound;
import com.danit.bank.exception.CustomerNotFound;
import com.danit.bank.exception.EmployerNotFound;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@Slf4j
@ControllerAdvice
public class GlobalControllerAdvice extends ResponseEntityExceptionHandler {
    @ExceptionHandler(CustomerNotFound.class)
    public ResponseEntity<Object> handleCustomerNotFoundException(CustomerNotFound ex) {
        log.warn(ex.getMessage(), ex);
        return new ResponseEntity<>(getBody(NOT_FOUND, ex, "Please enter a valid number customer"), new HttpHeaders(), NOT_FOUND);
    }

    @ExceptionHandler(AccountNotFound.class)
    public ResponseEntity<Object> handleAccountNotFoundException(AccountNotFound ex) {
        log.warn(ex.getMessage(), ex);
        return new ResponseEntity<>(getBody(NOT_FOUND, ex, "Please enter a valid number account"), new HttpHeaders(), NOT_FOUND);
    }

    @ExceptionHandler(EmployerNotFound.class)
    public ResponseEntity<Object> handleAccountNotFoundException(EmployerNotFound ex) {
        log.warn(ex.getMessage(), ex);
        return new ResponseEntity<>(getBody(NOT_FOUND, ex, "Please enter a valid number employer"), new HttpHeaders(), NOT_FOUND);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> exception(Exception ex) {
        log.error(ex.getMessage(), ex);
        return new ResponseEntity<>(getBody(INTERNAL_SERVER_ERROR, ex, "Something Went Wrong"), new HttpHeaders(), INTERNAL_SERVER_ERROR);
    }


    public Map<String, Object> getBody(HttpStatus status, Exception ex, String message) {
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("message", message);
        body.put("timestamp", new Date());
        body.put("status", status.value());
        body.put("error", status.getReasonPhrase());
        body.put("exception", ex.toString());

        Throwable cause = ex.getCause();
        if (cause != null) {
            body.put("exceptionCause", ex.getCause().toString());
        }
        return body;
    }
}