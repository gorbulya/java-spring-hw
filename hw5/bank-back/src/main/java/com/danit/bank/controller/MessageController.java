package com.danit.bank.controller;

import com.danit.bank.model.RequestMessage;
import com.danit.bank.model.ResponseMessage;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

@Slf4j
@Controller
@RequiredArgsConstructor
public class MessageController {
    private final SimpMessagingTemplate template;

    @MessageMapping("/message")
    @SendTo("/chat/messages")
    public ResponseMessage getMessages(ResponseMessage message) {
        System.out.println(message);
        return message;
    }

}
