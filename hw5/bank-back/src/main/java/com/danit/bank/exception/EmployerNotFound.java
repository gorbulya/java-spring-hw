package com.danit.bank.exception;

public class EmployerNotFound extends Exception {
    public EmployerNotFound(String message) {
        super(message);
    }
}